#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
from Utility import *
from Git import Git
import re

class CheckModules:
    def __init__(self, modules = {}, git = Git()):
        self.git = git
        self._gitdir = git.env['GIT_DIR']
        self._modules = modules
        self._modline = re.compile(r':160000 160000 ' +
                                   r'(?P<src_obj>[0-9a-z]{40}) ' +
                                   r'(?P<dst_obj>[0-9a-z]{40}) ' +
                                   r'(?P<status>[ADMTUX])' + # [CR]
                                   r'	(?P<name>.*)')

    def _check_repo(self, commit, path, obj):
        # Check if this path matches a known submodule.
        if not self._modules.has_key(path):
            return
        mod = self._modules[path]
        self.git.env['GIT_DIR'] = mod.gitdir

        # Make sure the commit exists in the submodule repository.
        code, out, err = self.git('cat-file', '-t', obj, safe = True)
        unreachable = False
        if code != 0 or out.strip() != 'commit':
            unreachable = True
        elif mod.branch:
            # The commit must be in the given branch of the submodule repo.
            code, base, err = self.git('merge-base', obj, mod.branch, safe = True)
            if code != 0 or base.strip() != obj:
                unreachable = True
        else:
            # The commit must be in at least one branch of the submodule repo.
            unreachable = True
            revs = self.git('for-each-ref', '--format=%(objectname)', 'refs/heads/')
            for rev in revs.splitlines():
                code, base, err = self.git('merge-base', obj, rev, safe = True)
                if code == 0 and base.strip() == obj:
                    unreachable = False
        if unreachable:
            if mod.branch: place = """ '%s' at""" % mod.branch
            else: place = ''
            fail("""commit %s references unreachable submodule commit %s at
  %s
Push the submodule commit to%s
  %s
first!""" % (commit.sha1[0:8], obj[0:8], path, place, mod.url))

        # Make sure the commit cannot see any denied commits.
        if mod.refs_deny:
            refs = self.git('for-each-ref',
                            '--format=%(objectname) %(refname)',
                            mod.refs_deny+'/')
            for line in refs.splitlines():
                rev, ref = line.split()
                base = self.git('merge-base', rev, obj).strip()
                if base == rev:
                    fail("""commit %s references submodule commit %s at
  %s
The submodule commit descends from commit %s that is
denied by the submodule repository at
  %s
by
  %s""" % (commit.sha1[0:8], obj[0:8], path, rev[0:8], mod.url, ref))

    def _check_rewind(self, commit, path, src_obj, dst_obj):
        # Check that the submodule update is not a rewind from any parent.
        mod = self._modules[path]
        self.git.env['GIT_DIR'] = mod.gitdir
        code, out, err = self.git('merge-base', src_obj, dst_obj, safe = True)
        if code == 0 and out.strip() == dst_obj:
            self.git.env['GIT_DIR'] = self._gitdir
            parents = ''
            for i in range(len(commit.parents)):
                code, out, err = self.git('rev-parse', '%s:%s' % (commit.parents[i], path), safe = True)
                if code == 0:
                    parents = (parents + '\n  parent %s^%d (%s) references submodule commit %s' %
                               (commit.sha1[0:8], i+1, commit.parents[i][0:8], out[0:8]))
            fail("""commit %s rewinds the submodule at
  %s
from commit %s to its ancestor %s%s""" %
                (commit.sha1[0:8], path, src_obj[0:8], dst_obj[0:8],
                 (':'+parents) if parents else '.'))

    def enforce(self, commit, user):
        # Check that submodule commits exist.
        for diff in commit.diffs():
            if diff['dst_mode'] == '160000':
                self._check_repo(commit, diff['name'], diff['dst_obj'])

        # Look for submodules different from at least one parent.
        self.git.env['GIT_DIR'] = self._gitdir
        module_names = sorted(self._modules.keys())
        module_diffs = self.git('diff-tree', '--root', '-m', '-r', '--no-commit-id',
                                commit.sha1, '--', *module_names)
        for line in module_diffs.splitlines():
            diff = self._modline.match(line)
            if diff:
                d = diff.groupdict()
                self._check_rewind(commit, d['name'], d['src_obj'], d['dst_obj'])
