#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import os
import subprocess

class GitError(Exception):
    def __init__(self, value): self.value = value
    def __str__(self): return repr(self.value)

class Git:
    def __init__(self, git = 'git'):
        self.git = git
        self.env = {}
        for key, value in os.environ.items():
            self.env[key] = value

    def __call__(self, *args, **keys):
        pipes = {'stdout': subprocess.PIPE,
                 'stderr': subprocess.PIPE}
        stdin = None
        if keys.has_key('stdin'):
            stdin = keys['stdin']
            pipes['stdin'] = subprocess.PIPE
        p = subprocess.Popen([self.git] + list(args),
                             env = self.env, **pipes)
        result = p.communicate(stdin)
        if keys.has_key('safe') and keys['safe']:
            return p.returncode, result[0], result[1]
        elif p.returncode != 0:
            raise GitError(result[1])
        else:
            return result[0]
