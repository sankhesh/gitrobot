#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import sys
import subprocess

from Git import GitError

class Fail(Exception):
    def __init__(self, msg): self.msg = msg
    def __str__(self): return self.msg

def fail(msg):
    raise Fail(msg)

def info(msg):
    """Display the given message."""
    sys.stdout.write(msg + '\n')
    sys.stdout.flush()

def die_plain(msg):
    """Display the given message and exit with error."""
    sys.stderr.write("%s\n" % msg)
    sys.exit(1)

def die(msg):
    """Display the given message and exit with error."""
    sys.stderr.write("""----------------------------------------------------------------------
%s
----------------------------------------------------------------------\n""" % msg)
    sys.exit(1)

def git(*args):
    """Run a git command.  Returns the output.  Exception on error."""
    cmd = ['git'] + list(args)
    p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    result = p.communicate()
    if p.returncode != 0: raise GitError(result[1])
    return result[0]

def git_safe(*args):
    """Run a git command.  Returns result code and output."""
    cmd = ['git'] + list(args)
    p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    output = p.communicate()
    return p.returncode, output[0], output[1]

zero = '0000000000000000000000000000000000000000'
