#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
from Utility import *

class CheckMessage:
    def __init__(self, minfirst = 8, maxfirst = 78, message = None, no_change_id = False, allow_fixup = False):
        self._minfirst = minfirst
        self._maxfirst = maxfirst
        self._message = message or ''
        self._no_change_id = no_change_id
        self._allow_fixup = allow_fixup

    def _bad_log(self, commit, err):
        fail('commit %s has an invalid commit message:\n  %s\ncommit message is:\n%s%s' %
             (commit.sha1[0:8], err, commit.log.rstrip('\n'), self._message))

    def _is_auto_msg(self, first):
        return first.startswith(('Merge ', 'Revert '))

    def enforce(self, commit, user):
        lines = commit.log.splitlines()
        if len(lines) == 0:
            self._bad_log(commit, "the message cannot be empty")
        first = lines[0]
        if len(first) < self._minfirst:
            self._bad_log(commit, "the first line must be at least %d characters" % self._minfirst)
        elif self._maxfirst and len(first) > self._maxfirst and not self._is_auto_msg(first):
            self._bad_log(commit, "the first line must not be more than %d characters" % self._maxfirst)
        elif first.strip() != first:
            self._bad_log(commit, "the first line must not have leading or trailing whitespace")
        if len(lines) > 1:
            if len(lines[1]) > 0:
                self._bad_log(commit, "the second line must be empty (if it exists)")
            if len(lines) == 2:
                self._bad_log(commit, "the message cannot have exactly two lines")
        if len(lines) > 2:
            if len(lines[2]) == 0:
                self._bad_log(commit, "the third line must not be empty (if it exists)")
        shortsha = commit.sha1[:8]
        if first.startswith('WIP'):
            fail("commit %s cannot be merged because it is marked as Work-In-Progress (WIP)" %
                 (shortsha))
        if not self._allow_fixup and first.startswith('fixup! '):
            fail("commit %s cannot be merged because it is marked as a fixup commit" %
                 (shortsha))
        if not self._allow_fixup and first.startswith('squash! '):
            fail("commit %s cannot be merged because it is marked as commit to be squashed" %
                 (shortsha))
        if self._no_change_id and [l for l in lines if l.startswith('Change-Id: ')]:
            self._bad_log(commit, "the message must not have a Change-Id")
